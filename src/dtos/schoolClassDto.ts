export class SchoolClassDto {
    id?: string;
    startYear?: number;
    classLevel?: string;
    name?: string;
    normalizedSchoolClassName?: string;
}