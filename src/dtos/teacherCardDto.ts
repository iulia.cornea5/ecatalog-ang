export class TeacherCardDto {
    id?: string;
    firstName?: string;
    lastName?: string;
    subject?: string;
    profilePicture?: string;
}