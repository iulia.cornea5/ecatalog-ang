import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchoolClassesTableComponent } from './school-classes-table/school-classes-table.component';
import { TeacherCardListComponent } from './teacher-card-list/teacher-card-list.component';
import { TeacherFormComponent } from './teacher-form/teacher-form.component';
import { UserLoginPageComponent } from './user-login-page/user-login-page.component';

const routes: Routes = [
  {path: 'classes', component: SchoolClassesTableComponent},
  {path: 'teachers', component: TeacherCardListComponent},
  {path: 'new-teacher', component: TeacherFormComponent},
  {path: 'login', component: UserLoginPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
