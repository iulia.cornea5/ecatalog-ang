import { Component, Input } from '@angular/core';
import { TeacherCardDto } from 'src/dtos/teacherCardDto';

@Component({
  selector: 'app-teacher-card',
  templateUrl: './teacher-card.component.html',
  styleUrls: ['./teacher-card.component.css']
})
export class TeacherCardComponent {

  @Input()
  teacher: TeacherCardDto = {
    id: "",
    firstName: "",
    lastName: "",
    subject: "",
    profilePicture: ""
  };


}
