import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { SchoolClassDto } from 'src/dtos/schoolClassDto';

@Component({
  selector: 'app-school-classes-table',
  templateUrl: './school-classes-table.component.html',
  styleUrls: ['./school-classes-table.component.css']
})
export class SchoolClassesTableComponent {

  partialColumns = ['id', 'normalizedSchoolClassName', 'startYear', 'classLevel'];
  dataSource: SchoolClassDto[] = [];

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {
    // http://localhost:8080/school-class ---> [{}, {}, ... {}]
    this.httpClient.get("/api/school-class").subscribe(response => {
      this.dataSource =  response as SchoolClassDto[];
    });

  }

}
